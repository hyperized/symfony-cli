FROM alpine

RUN apk update && \
    apk upgrade && \
    apk add bash git wget

RUN wget https://get.symfony.com/cli/installer && \
    bash installer --install-dir=/usr/bin